package com.usanin.stock;

import com.usanin.stock.websocket.WebSocketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;

@SpringBootApplication
@EnableScheduling
public class StockRealtimeAggregatorApplication {
    @Value("${websocket.poligon.url}")
    private String url;

    public static void main(String[] args) {
        SpringApplication.run(StockRealtimeAggregatorApplication.class, args);
    }

    @Autowired
    private WebSocketManager webSocketManager;

    @Bean()
    public WebSocketConnectionManager wsConnectionManager() {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.setDefaultMaxBinaryMessageBufferSize(150000);
        container.setDefaultMaxTextMessageBufferSize(150000);
        WebSocketConnectionManager manager = new WebSocketConnectionManager(
                new StandardWebSocketClient(container),
                webSocketManager,
                this.url);
        manager.start();

        manager.setAutoStartup(true);

        return manager;
    }

}
