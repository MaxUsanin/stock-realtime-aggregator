package com.usanin.stock.rest;

import com.usanin.stock.storage.StockRealtimeStorage;
import com.usanin.stock.storage.entities.SMA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ReportsController {

    @Autowired
    private StockRealtimeStorage storage;

    @GetMapping(value = "/sma/{interval}")
    public ResponseEntity<Map<String, Double>> sma(@PathVariable(name = "interval") int interval) {
        return new ResponseEntity<>(storage.sma(interval)
                .parallelStream()
                .collect(Collectors.toMap(SMA::getSymbol, SMA::getValue)), HttpStatus.OK);
    }
}
