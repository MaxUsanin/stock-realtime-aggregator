package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.SMA;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SMARowMapper implements RowMapper<SMA> {
    @Override
    public SMA mapRow(ResultSet rs, int i) throws SQLException {
        return SMA.builder()
                .symbol(rs.getString("SYMBOL"))
                .value(rs.getDouble("VALUE"))
                .build();
    }
}
