package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.StockRealtimeData;
import com.usanin.stock.websocket.PoligonDataMessage;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StockRealtimeConverter {

    public StockRealtimeData convert(PoligonDataMessage message) {

        return StockRealtimeData.builder()
                .datetime(new Date(message.getS()))
                .vol(message.getV())
                .close(message.getC())
                .open(message.getO())
                .high(message.getH())
                .low(message.getL())
                .build();
    }
}
