package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.StockRealtimeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public interface StockRealtimeDataRepository extends JpaRepository<StockRealtimeData, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE FROM StockRealtimeData s WHERE s.datetime <= :creationDateTime")
    void truncateOldData(@Param("creationDateTime") Date creationDateTime);
}
