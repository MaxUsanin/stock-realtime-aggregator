package com.usanin.stock.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class StockRealtimeOptimizer {

    @Autowired
    private StockRealtimeStorage storage;


    @Scheduled(fixedRate = 1000*60*60, initialDelay = 5000)
    private void truncateOldData() {
        storage.truncateOldData();
    }
}
