package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.SMA;
import com.usanin.stock.storage.entities.StockRealtimeData;

import java.util.List;

public interface StockRealtimeStorage {
    List<SMA> sma(Integer interval);
    void truncateOldData();
    void put(String symbol, StockRealtimeData realtimeDataList);
}
