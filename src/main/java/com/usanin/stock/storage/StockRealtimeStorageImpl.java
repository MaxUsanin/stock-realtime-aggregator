package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.SMA;
import com.usanin.stock.storage.entities.StockRealtimeData;
import com.usanin.stock.storage.entities.StockSymbol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class StockRealtimeStorageImpl implements StockRealtimeStorage {

    private static final Map<String, StockSymbol> stockSymbolMap = new HashMap<>();

    @Autowired
    private StockSymbolRepository stockSymbolRepository;

    @Autowired
    private StockRealtimeDataRepository stockRealtimeDataRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void init() {
        jdbcTemplate.setFetchSize(10000);
    }


    private final static String SMA_QUERY = "select\n" +
            "   ss.SYMBOL, (SUM(sr.P_CLOSE) / COUNT(*)) AS value\n" +
            "FROM STOCK_SYMBOLS ss \n" +
            "   left join\n" +
            "      ( select\n" +
            "            SYMBOL,\n" +
            "            DATETIME,\n" +
            "            P_CLOSE,\n" +
            "            dense_rank() over (partition by SYMBOL \n" +
            "         order by\n" +
            "            DATETIME desc) rank \n" +
            "         from\n" +
            "            STOCK_REALTIME ) sr \n" +
            "      on ss.SYMBOL = sr.SYMBOL \n" +
            "where\n" +
            "   rank <= ? \n" +
            "GROUP BY\n" +
            "   ss.SYMBOL\n" +
            "order by SYMBOL";

    @Override
    public List<SMA> sma(Integer interval) {
        return jdbcTemplate.query(SMA_QUERY, new SMARowMapper(), interval);
    }

    public void truncateOldData() {
        var date = Date.from(LocalDateTime.now()
                .minusDays(4)
                .atZone(ZoneId.systemDefault())
                .toInstant());
        stockRealtimeDataRepository.truncateOldData(date);
        stockSymbolRepository.truncateOldData();
    }

    public void put(String symbol, StockRealtimeData data) {

        if (stockSymbolMap.containsKey(symbol)) {
            data.setSymbol(stockSymbolMap.get(symbol));
        } else {
            stockSymbolRepository.findById(symbol).ifPresentOrElse(stockSymbol -> {
                data.setSymbol(stockSymbol);
                stockSymbolMap.put(symbol, stockSymbol);
            }, () ->
            {
                var stockSymbol = StockSymbol.builder().symbol(symbol).realtimeData(new ArrayList<>()).build();
                stockSymbolRepository.save(stockSymbol);
                data.setSymbol(stockSymbol);
                stockSymbolMap.put(symbol, stockSymbol);
            });
        }
        stockRealtimeDataRepository.save(data);
    }
}
