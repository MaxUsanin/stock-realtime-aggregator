package com.usanin.stock.storage;

import com.usanin.stock.storage.entities.StockSymbol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface StockSymbolRepository extends JpaRepository<StockSymbol, String> {

    @Transactional
    @Modifying
    @Query("DELETE FROM StockSymbol SS WHERE SS.symbol NOT IN (SELECT SR.symbol FROM StockRealtimeData SR)")
    void truncateOldData();
}
