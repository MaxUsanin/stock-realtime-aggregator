package com.usanin.stock.storage.entities;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SMA {
    private String symbol;
    private Double value;
}
