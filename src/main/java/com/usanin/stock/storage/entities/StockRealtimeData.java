package com.usanin.stock.storage.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@ToString
@Builder
@Setter
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STOCK_REALTIME")
public class StockRealtimeData implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TXT_ID")
    private int txnId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SYMBOL", nullable = false)
    private StockSymbol symbol;


    @Column(name = "DATETIME", nullable = false)
    private Date datetime;

    @Column(name = "P_OPEN")
    private double open;

    @Column(name = "P_CLOSE")
    private double close;

    @Column(name = "P_HIGH")
    private double high;

    @Column(name = "P_LOW")
    private double low;

    @Column(name = "VOL")
    private int vol;
}
