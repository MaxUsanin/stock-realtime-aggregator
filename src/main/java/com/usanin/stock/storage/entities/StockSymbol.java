package com.usanin.stock.storage.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STOCK_SYMBOLS")
public class StockSymbol implements Serializable {

    @Id
    @Setter
    @Getter
    private String symbol;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "SYMBOL")
    @Setter
    @Getter
    private List<StockRealtimeData> realtimeData;

}
