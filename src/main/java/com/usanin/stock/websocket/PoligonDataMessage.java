package com.usanin.stock.websocket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PoligonDataMessage {
    private String ev;
    private String sym;
    private Integer v;
    private Integer av;
    private Double op;
    private Double vw;
    private Double o;
    private Double c;
    private Double h;
    private Double l;
    private Double a;
    private Integer z;
    private Long s;
    private Long e;
}
