package com.usanin.stock.websocket;


import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketConnectionManager;

import java.io.IOException;

@Service
public class PoligonWebSocketCommand {
    @Setter
    private WebSocketSession session;

    @Value("${websocket.poligon.url}")
    private String url;


    @Value("${websocket.poligon.token}")
    private String token;

    @Value("${websocket.poligon.subscripion.tickers}")
    private String tickers;

    @Autowired
    private ApplicationContext appContext;

    public void reconnect() throws InterruptedException {
        WebSocketConnectionManager connectionManager = appContext.getBean("connectionManager", WebSocketConnectionManager.class);
        connectionManager.stop();
        Thread.sleep(1000 * 60 * 5);
        connectionManager.start();
        connectionManager.setAutoStartup(true);
    }


    public void subscribe() throws IOException {
        session.sendMessage(new TextMessage("{\"action\":\"subscribe\", \"params\":\""+tickers+"\"}"));

    }


    public void authorize() throws IOException {
        session.sendMessage(new TextMessage("{\"action\":\"auth\", \"params\":\"" + token + "\"}"));
    }
}
