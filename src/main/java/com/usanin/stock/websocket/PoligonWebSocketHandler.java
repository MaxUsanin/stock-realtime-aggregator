package com.usanin.stock.websocket;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.util.List;
import java.util.Optional;

public abstract class PoligonWebSocketHandler extends AbstractWebSocketHandler {
    final ObjectMapper objectMapper = new ObjectMapper();

    public PoligonWebSocketHandler() {
    }

    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        var firstLine = new Gson().fromJson(message.getPayload(), JsonArray.class).get(0).getAsJsonObject();
        var ev = firstLine.get("ev").getAsString();

        if ("status".equals(ev)) {
            var status = Optional.ofNullable(firstLine.get("status")).map(JsonElement::getAsString).orElse("");
            var statusMessage = Optional.ofNullable(firstLine.get("message")).map(JsonElement::getAsString).orElse("");
            if ("auth_success".equals(status) && "authenticated".equals(statusMessage)) {
                this.authorized(session);
            }
        } else {
            this.handleMessages(session, objectMapper.readValue(message.getPayload(), new TypeReference<>() {}));
        }
    }

    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        this.error(session, exception);
    }

    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        this.disconnected(session, status);
    }

    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        this.connected(session);
    }

    public boolean supportsPartialMessages() {
        return false;
    }

    abstract void handleMessages(WebSocketSession session, List<PoligonDataMessage> message) throws Exception;

    abstract void authorized(WebSocketSession session) throws Exception;

    abstract void connected(WebSocketSession session) throws Exception;

    abstract void disconnected(WebSocketSession session, CloseStatus status) throws Exception;

    abstract void error(WebSocketSession session, Throwable exception) throws Exception;

}
