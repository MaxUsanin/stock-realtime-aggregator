package com.usanin.stock.websocket;

import com.usanin.stock.storage.StockRealtimeConverter;
import com.usanin.stock.storage.StockRealtimeStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;


@Service
public class WebSocketManager extends PoligonWebSocketHandler {

    private static final Logger logger
            = LoggerFactory.getLogger(WebSocketManager.class);

    @Autowired
    private PoligonWebSocketCommand webSocketCommand;

    @Autowired
    private StockRealtimeStorage storage;

    @Autowired
    private StockRealtimeConverter converter;


    @Override
    void handleMessages(WebSocketSession session, List<PoligonDataMessage> message) {
        message.stream().parallel().forEach(m -> {
            storage.put(m.getSym(), converter.convert(m));
        });
    }

    @Override
    void authorized(WebSocketSession session) throws Exception {
        logger.info("Was authorized");
        webSocketCommand.subscribe();
    }

    @Override
    void connected(WebSocketSession session) throws Exception {
        logger.info("Was connected");
        webSocketCommand.setSession(session);
        webSocketCommand.authorize();
    }

    @Override
    void disconnected(WebSocketSession session, CloseStatus status) throws Exception {
        logger.info("Was disconnected " + status.getReason());
        webSocketCommand.reconnect();

    }

    @Override
    void error(WebSocketSession session, Throwable exception) throws Exception {
        logger.info("Was error " + exception.getMessage());
        webSocketCommand.reconnect();
    }
}