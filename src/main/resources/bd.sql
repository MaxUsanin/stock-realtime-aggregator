CREATE TABLE stock_realtime
(
    symbol VARCHAR2(9) NOT NULL,
    txt_id NUMBER generated always AS identity MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1
        START WITH 1 CACHE 20 NOORDER NOCYCLE nokeep noscale NOT NULL ENABLE,
    CONSTRAINT "TRANSACTION_PK" PRIMARY KEY (txt_id),
    CONSTRAINT multy_constr UNIQUE (symbol, datetime),
    CONSTRAINT fk_symbol FOREIGN KEY (symbol) REFERENCES stock_symbols(symbol),
    datetime DATE NOT NULL,
    p_open FLOAT,
    p_close FLOAT ,
    p_high FLOAT ,
    p_low FLOAT ,
    vol INT
)

CREATE TABLE stock_symbols
(
    symbol VARCHAR2(9) NOT NULL,
    CONSTRAINT symbol_pk PRIMARY KEY (symbol)
)

